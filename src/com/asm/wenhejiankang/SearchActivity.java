package com.asm.wenhejiankang;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.asm.wenhejiankang.bluetooth.JKBluetoothManager;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import com.asm.wenhejiankang.bluetooth.OnStateChangedListener;
import java.util.List;
import android.widget.Adapter;
import com.xl.view.ListAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import com.asm.wenhejiankang.bluetooth.OnBloudOxygenDataChangedListener;
/*

设备搜索界面
风的影子
*/
public class SearchActivity extends StartActivity implements OnStateChangedListener , AdapterView.OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> p1, View p2, int pos, long p4)
			{
				// TODO: Implement this method
			if(manager.getState()!=JKBluetoothManager.STATE_DISCOVERIED)
				return;
			manager.connect(list_dev.get(pos));
				setResult(RESULT_OK);
				finish();
			}
		
		public static final String TAG="SearchActivity";
		@Override
		public void onStateChanged(List<BluetoothDevice> devices, int s)
			{
			android.util.Log.d("xl.state", "xl:"+s);
				if(s==JKBluetoothManager.STATE_FOUND){
				Log.e(TAG, "搜索到一个设备");
				if(devices!=null)
				{
				adapter.clear();
				list_dev = devices;
				for(BluetoothDevice d:devices)
				adapter.add(d.getName(),d.getAddress());
				adapter.notifyDataSetChanged();
				}else if(s==JKBluetoothManager.STATE_DISCOVERIED){
				setProgressVisiblity(View.GONE);
					}
				}
			}
			

		//
		JKBluetoothManager manager;
		//控制进度条的显示和隐藏
		LinearLayout layout_progress;
		ListView listview;
		Button reBt,stBt;
    ListAdapter adapter;
		List<BluetoothDevice>  list_dev;
		
		@Override
		protected void onCreate(Bundle savedInstanceState)
			{
				// TODO: Implement this method
				super.onCreate(savedInstanceState);
			}

			
			
		
		@Override
		public void onContentView()
			{
				// TODO: Implement this method
				super.onContentView();
				setContentView(R.layout.search);
				layout_progress = (LinearLayout) findViewById(R.id.search_progress);
				listview = (ListView)findViewById(R.id.searchListView);
				reBt=(Button)findViewById(R.id.search_reBt);
				stBt=(Button)findViewById(R.id.search_stBt);
				reBt.setOnClickListener(new Button.OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO 自动生成的方法存根
						manager.discoveryValidDevices();
					}
				});
				stBt.setOnClickListener(new Button.OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO 自动生成的方法存根
						manager.cancleDiscoveryValidDevices();
						setProgressVisiblity(View.GONE);
					}
				});
				listview.setOnItemClickListener(this);
				adapter = new ListAdapter(this);
				listview.setAdapter(adapter);
				manager =JKBluetoothManager.getInstance(this);
				manager.setOnStateChangedListener(this);
				manager.discoveryValidDevices();
			}
			
		//隐藏搜索进度条
		void setProgressVisiblity(int type)
		{
			layout_progress.setVisibility(type);
		}

}
